package com.information.springboot.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Student_info")
public class Information {
	
   @Id
   private String id;
   private String firstname;
   private String lastname;
   private String mobile_no;
   private List<Address> address;
	
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getMobile_no() {
	return mobile_no;
}
public void setMobile_no(String mobile_no) {
	this.mobile_no = mobile_no;
}
public List<Address> getAddress() { 
return address; 
} 

public void setAddress(List<Address> address) { 
	this.address = address; 
	}
   
}
