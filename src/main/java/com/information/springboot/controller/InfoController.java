package com.information.springboot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.information.springboot.model.Information;
import com.information.springboot.service.InfoService;

@RestController
public class InfoController {
   
     @Autowired
	 private InfoService infoService;
     @GetMapping("/all/information")
     public ResponseEntity<List<Information>> getAllInfo() {
    	 return ResponseEntity.ok().body(infoService.getAllInfo());
     }
     
     @PostMapping("/create/informaton")
     public ResponseEntity<Information> createInfo(@RequestBody Information information) {
    	 return ResponseEntity.ok().body(this.infoService.createInfo(information));
     }
     
     @GetMapping("/id/information/{id}")
     public ResponseEntity<Optional<Information>> getInfoById(@PathVariable String id) {
    	 return ResponseEntity.ok().body(this.infoService.getInfoById(id));
     }
     @PutMapping("/update/information/{id}")
     public ResponseEntity<Information> updateInfo(@PathVariable String id,@RequestBody Information information) {
    	 information.setId(id);
    	 return ResponseEntity.ok().body(this.infoService.updateInfo(id,information));
     }
     
     @DeleteMapping("/delete/information/{id}")
     public String deleteById(@PathVariable ("id") String id) {
    			 this.infoService.deleteById(id);
    			 return "Deleted Row";
     }
}
