package com.information.springboot.service;

import java.util.List;
import java.util.Optional;

import com.information.springboot.model.Information;

public interface InfoService {
	Information createInfo(Information information);
	Information updateInfo(String id,Information information);
	List<Information> getAllInfo();
	Optional<Information> getInfoById(String id);
	void deleteById(String id);
	
}
