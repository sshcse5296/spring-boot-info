package com.information.springboot.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.information.springboot.exception.ResourceNotFoundException;
import com.information.springboot.model.Address;
import com.information.springboot.model.Information;
import com.information.springboot.repository.InfoRepository;
@Service
public class InfoServiceImpl implements InfoService {
      
    @Autowired
    private InfoRepository infoRepository;
    
	@Override
	public Information createInfo(Information information) {
		return infoRepository.save(information);
	}

	@Override
	public Information updateInfo(String id,Information information) {
		Information exitInfoId=this.infoRepository.findById(id)
				.orElseThrow(()->new ResourceNotFoundException("Id Not Exit ="+id));
		exitInfoId.setId(information.getId());
		exitInfoId.setFirstname(information.getFirstname());
		exitInfoId.setLastname(information.getLastname());
		exitInfoId.setMobile_no(information.getMobile_no());
		List<Address> adr=information.getAddress().stream().map(td->{
			td.setId(td.getId());
			td.setVillege(td.getVillege());
			td.setPost_offic(td.getPost_offic());
			td.setDistrict(td.getDistrict());
			td.setState(td.getState());
			td.setPincode(td.getPincode());
			return td;
		}).collect(Collectors.toList());
		exitInfoId.setAddress(adr);
		
		infoRepository.save(exitInfoId);
		return (Information) exitInfoId;
	}

	@Override
	public List<Information> getAllInfo() {
		
		return this.infoRepository.findAll();
	}

	@Override
	public Optional<Information> getInfoById(String id) {
		Optional<Information>  infoId=this.infoRepository.findById(id);
		if(infoId.isPresent()) {
			return this.infoRepository.findById(id);
		}
		else {
			throw new ResourceNotFoundException("Id Not Exit ="+id);
		}
	}

	@Override
	public void deleteById(String id) {
		Information exitInfoId=this.infoRepository.findById(id)
				.orElseThrow(()->new ResourceNotFoundException("Id Not Exit ="+id));
		this.infoRepository.delete(exitInfoId);
	}

}
