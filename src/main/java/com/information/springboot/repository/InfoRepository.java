package com.information.springboot.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.information.springboot.model.Information;

@Repository
public interface InfoRepository extends MongoRepository<Information, Integer>{

	Optional<Information> findById(String id);

}
